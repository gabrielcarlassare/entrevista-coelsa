public class Contact
{

    public Contact()
    {

    }

    public Contact(string _firstName, string _lastName, string _email, int _phoneNumber, int _companyId)
    {
        this.Id = 0;
        this.FirstName = _firstName;
        this.LastName = _lastName;
        this.Email = _email;
        this.PhoneNumber = _phoneNumber;
        this.CompanyId = _companyId;
    }

    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public int PhoneNumber { get; set; }
    public int CompanyId { get; set; }
    public Company Company { get; set; }

}
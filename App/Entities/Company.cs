using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

public class Company
{
    public int Id { get; set; }
    public string Name { get; set; }
    public List<Contact> Contacts { get; set; }

}
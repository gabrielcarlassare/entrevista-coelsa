using AutoMapper;

public class ViewModelToDomainMappingProfile : Profile
{
    public ViewModelToDomainMappingProfile()
    {
        CreateMap<ContactViewModel, Contact>();
        CreateMap<CompanyViewModel, Company>();
        CreateMap<ContactsPageViewModel, ContactsPage>();
    }
}
using AutoMapper;

public class DomainToViewModelMappingProfile : Profile
{
    public DomainToViewModelMappingProfile()
    {

        CreateMap<Contact, ContactViewModel>();
        CreateMap<Company, CompanyViewModel>();
        CreateMap<ContactsPage, ContactsPageViewModel>();

    }

}
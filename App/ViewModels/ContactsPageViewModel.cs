using System.Collections.Generic;

public class ContactsPageViewModel
{
    public int TotalPages { get; set; }
    public int CurrentPage { get; set; }
    public List<Contact> Contacts { get; set; }
}
using System.Collections.Generic;

public class ContactsPage
{
    public ContactsPage(int page, List<Contact> contacts, int totalPages)
    {
        this.CurrentPage = page;
        this.Contacts = contacts;
        this.TotalPages = totalPages;
    }

    public int TotalPages { get; set; }
    public int CurrentPage { get; set; }
    public List<Contact> Contacts { get; set; }
}
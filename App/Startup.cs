using System;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Gaby
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<ApplicationDbContext>(
            //     options =>
            // {
            //     var connection = Configuration.GetConnectionString("dBConnection");

            //     options.UseSqlServer(connection);
            // }
            );

            services
            .AddTransient<IContactRepository, ContactRepository>()
            .AddTransient<IUnitOfWork, UnitOfWork>()
            .AddTransient<IHelpers, Helpers>();

            services
            .Configure<Settings>(Configuration.GetSection("Settings"))
            .Configure<Messages>(Configuration.GetSection("Messages"));

            services.AddCors(options =>
                {
                    options.AddPolicy(name: MyAllowSpecificOrigins,
                        builder =>
                            {
                                builder.AllowAnyOrigin()
                                    .AllowAnyMethod()
                                    .AllowAnyHeader();
                            });
                });

            var mappingConfig = new MapperConfiguration(mc =>
        {
            mc.AddProfile(new DomainToViewModelMappingProfile());
            mc.AddProfile(new ViewModelToDomainMappingProfile());
        });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors(MyAllowSpecificOrigins);

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

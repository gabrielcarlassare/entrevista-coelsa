using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class ContactConfiguration : IEntityTypeConfiguration<Contact>
{
    public void Configure(EntityTypeBuilder<Contact> builder)
    {
        builder.Property(contact => contact.Id).ValueGeneratedOnAdd();

        builder.HasKey(contact => new
        {
            contact.Id
        });

        builder
                .Property(contact => contact.FirstName)
                .HasColumnType("nvarchar(70)")
                .IsRequired();

        builder.Property(contact => contact.LastName)
                .HasColumnType("nvarchar(70)")
                .IsRequired();

        builder.Property(contact => contact.Email)
                .HasColumnType("nvarchar(100)")
                .IsRequired();

        builder.HasOne(contact => contact.Company)
                .WithMany(c => c.Contacts)
                .HasForeignKey(contact => contact.CompanyId);
    }
}
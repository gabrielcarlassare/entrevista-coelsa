using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class CompanyConfiguration : IEntityTypeConfiguration<Company>
{
    public void Configure(EntityTypeBuilder<Company> builder)
    {
        builder.Property(company => company.Id).ValueGeneratedOnAdd();

        builder.HasKey(company => new
        {
            company.Id
        });

        builder.Property(c => c.Name)
                .HasColumnType("nvarchar(100)")
                .IsRequired();
    }
}
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace Application.Tests
{

    public class ContactControllerTests : IClassFixture<WebApplicationFactory<Gaby.Startup>>
    {
        public HttpClient _client { get; }

        public ContactControllerTests(WebApplicationFactory<Gaby.Startup> fixture)
        {
            _client = fixture.CreateClient();
        }


        [Fact]
        public async Task Object_Contact_Empty_Get_Should_Return_Error()
        {
            Contact contact = new Contact();

            var todoItemJson = new StringContent(
                JsonConvert.SerializeObject(contact),
                Encoding.UTF8,
                "application/json");

            var response = await _client.PostAsync("/contact/Add", todoItemJson);
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Get_Should_Return_Success()
        {
            Contact contact = new Contact("Juan", "Lopez", "juanlopez@gmail.com", 1234567890, 1);

            var todoItemJson = new StringContent(
                JsonConvert.SerializeObject(contact),
                Encoding.UTF8,
                "application/json");

            var response = await _client.PostAsync("/contact/Add", todoItemJson);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task Company_Undefined_Get_Should_Return_Error()
        {
            Contact contact = new Contact();
            contact.Id = 0;
            contact.FirstName = "dddd";
            contact.LastName = "eeeee";
            contact.Email = "prueba@gmail.com";
            contact.PhoneNumber = 1234567890;

            var todoItemJson = new StringContent(
                JsonConvert.SerializeObject(contact),
                Encoding.UTF8,
                "application/json");

            var response = await _client.PostAsync("/contact/Add", todoItemJson);
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

    }

}